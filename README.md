# lxf2ldr - Introduction

lxf2ldr is a command line tool to convert LEGO Digital Designer models (.LXF)
to LDraw models (.LDR/.MPD).

lxf2ldr uses LDD’s ldraw.xml conversion file (up-to-date version included) to
convert colours, ids, and positions/rotations.

lxf2ldr converts LDD’s groups into LDraw subfiles. (LDD does not do that. :P)

lxf2ldr can convert LDD’s instructions into LDraw steps (no groups in this case).

lxf2ldr also converts decorations (when possible/available in LDraw).

lxf2ldr converts flexible parts. (LDD does not do that anymore.) They are
exported as LSynth commands.

lxf2ldr is licensed under GPLv3+.

(ldraw.xml never had any explicit license and has been treated as “free to use
and modify,” so I guess is Public Domain or CC0.

About the same can be said about lsynth.mpd.)


# Alternative Project

If you can’t build from the sources, or don’t like or want to,
[lxf2ldr.html](https://gitlab.com/sylvainls/lxf2ldr.html), a sibling project,
is made for you.


# Links

About
[ldraw.xml](http://www.eurobricks.com/forum/index.php?/forums/topic/137193-more-up-to-date-ldrawxml-lddldraw-conversion-file/)
and LXF to LDR conversion.

# Build requirements

* Qt5 core
* QuaZIP (for Qt5)
* yaml-cpp


# Building

    git clone https://gitlab.com/sylvainls/lxf2ldr.git
    cd lxf2ldr
    qmake lxf2ldr.pro
    make


# Running

    lxf2ldr --ldrawxml where/is/ldraw.xml --decors where/is/decors_lxf2ldr.yaml mymodel.lxf > mymodel.ldr

All conversion files, `ldraw.xml` for colours, ids, and transformations,
`decors_lxf2ldr.yaml` for decorations, and `flex_lxf2ldr.yaml` for flexible
parts, are included in the binary. You can use other versions by giving their
locations on the command line (as on the example above).

Note that the LDraw result file is sent to the standard output, which you have
to redirect to a file.

lxf2ldr accepts multiple LXF files but the resulting MPD model will only show
the first model. All other models will be present but not shown/used.

Read help for other options.

    lxf2ldr --help


# GUI

A minimal graphical user interface in Qt is now available: `gui_l2l`. It’s
built in the `gui` directory.


# Notes

It’s highly recommended to remove unnecessary control points in the flexible
parts before running LSynth. It will greatly reduce the size of the file
LSynth will generate.

The included `lsynth.mpd` file adds Minifig Chain Link 5L (92338), Hose
Flexible 11.5L (14301), and Technic Tool Belt 17M (98567) as flexible parts.

If you want lxf2ldr to export flexible parts as one, unflexed, part (IOW, if
you don’t want to generate LSynth commands), you just need to remove the entry
in `flex_lxf2ldr.yaml` or totally replace it with an empty file
(e.g. `/dev/null`).

The same goes for decorations/patterns.

LEGO and LEGO Digital Designer are trademarks of the LEGO Group, which does
not sponsor, endorse, or authorize this application.
