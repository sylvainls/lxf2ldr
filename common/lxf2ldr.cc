/*
 *  Copyright 2017, 2020-2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lxf2ldr.h"

#include <QTextStream>
#include <QList>
#include <QMap>
#include <QVector3D>

#include "math_utils.h"
#include "lxf.h"
#include "ldrawxml.h"
#include "decors.h"
#include "flex.h"

static const QString LDR_ENDL = QStringLiteral( "\r\n" );

// helper function to format a number
QString double2str( const double& d )
{
    const static QString ZERO = QStringLiteral( "0" );
    const static int PREC = 6;
    constexpr static double TINY = std::pow( 10, -PREC );
    return std::abs( d ) < TINY ? ZERO  : QString::number( d, 'g', PREC );
}

// helper function to format a position vector
QString ldr_pos2str( const QVector3D& pos )
{
    const static QString FMT = QStringLiteral( "%1 %2 %3" );
    return FMT
        .arg( double2str( pos.x() ) )
        .arg( double2str( pos.y() ) )
        .arg( double2str( pos.z() ) );
}

// helper function to format a rotation matrix
QString ldr_mat2str( const QMatrix4x4& mat )
{
    const static QString FMT = QStringLiteral( "%1 %2 %3 %4 %5 %6 %7 %8 %9" );
    return FMT
        .arg( double2str( mat( 0, 0 ) ) )
        .arg( double2str( mat( 0, 1 ) ) )
        .arg( double2str( mat( 0, 2 ) ) )
        .arg( double2str( mat( 1, 0 ) ) )
        .arg( double2str( mat( 1, 1 ) ) )
        .arg( double2str( mat( 1, 2 ) ) )
        .arg( double2str( mat( 2, 0 ) ) )
        .arg( double2str( mat( 2, 1 ) ) )
        .arg( double2str( mat( 2, 2 ) ) );
}

// helper function to format the use of a part
QString ldr_use( int color, const QVector3D& position, const QMatrix4x4& rotation,
                 const QString& dat )
{
    // template for a brick
    const static QString FMT = QStringLiteral( "1 %1 %2 %3 %4" );
    return FMT.arg( color ).arg( ldr_pos2str( position ) ).arg( ldr_mat2str( rotation ) ).arg( dat );
}

// helper function to format a commented part
QString ldr_com( const QVector3D& position, const QMatrix4x4& rotation )
{
    // template for an unknown flexible part node
    const static QString FMT = QStringLiteral( "0 LXF2LDR FLEXNODE %1 %2" );
    return FMT.arg( ldr_pos2str( position ) ).arg( ldr_mat2str( rotation ) );
}

// helper function to compute and format a part/model
QString ldr( int color, const QString& lid,
             const QVector3D& ldd_pos, const QMatrix4x4& ldd_rot,
             std::optional<Transformation> x2l_tr,
             std::optional<Transformation> local_tr,
             bool commented=false )
{
    // change of basis matrix
    const static QMatrix4x4 AXES = rot2mat( 1, 0, 0, 180 );
    const static QVector3D ZERO;

    const QMatrix4x4 rot = x2l_tr ? ( ldd_rot * x2l_tr->rotation ) : ldd_rot;
    const QMatrix4x4 ldr_rot = AXES * ( local_tr ? ( rot * local_tr->rotation ) : rot ) * AXES;

    const QVector3D move = ( local_tr ? local_tr->translation : ZERO ) +
        ( x2l_tr ? x2l_tr->translation : ZERO );
    const QVector3D ldr_pos = AXES * ( ldd_pos + ( rot * move ) ) * 25;

    if ( commented ) {
        return ldr_com( ldr_pos, ldr_rot );
    } else {
        return ldr_use( color, ldr_pos, ldr_rot, lid );
    }
}

// helper function to compute and output a part
void part2ldr( const Part *const ldd, QTextStream& out, int mark_color,
               const LDrawXML& ldrawxml, const Decors& decors, const Flex& flexdb )
{
    static const QString FMT_SYNTH = QStringLiteral( "0 SYNTH BEGIN %1 %2\r\n" );
    static const QString SYNTH_PART = QStringLiteral( "ls01.dat" );
    static const QRegularExpression EMPTY_DECOR( "\\A0(,0)*\\Z" );

    Q_ASSERT( ldd );

    // translate id
    QString lid = ldrawxml.x2l_id( ldd->id );

    // transformation for that part
    std::optional<Transformation> x2l_tr = ldrawxml.x2l_tr( lid );

    // substitute part and color for decorations
    ColorSubstitute csub = decors.x2l_decor( ldd->id, ldd->decors, ldd->colors );
    if ( !csub.substitute.datfile.isEmpty() ) {
        lid = csub.substitute.datfile;
    }
    QList<int> colors;
    int main_color = ldrawxml.x2l_color( ldd->colors.first() );
    for ( int color: ldd->colors ) {
        colors << ( color ? ldrawxml.x2l_color( color ) : main_color ); // 0 means default/main
    }
    main_color = colors.at( csub.usecolor ); // ldr default isn’t first

    // marks unmatched decorated parts
    if ( mark_color != 0 && !ldd->decors.isEmpty() && csub.substitute.datfile.isEmpty() ) {
        const QRegularExpressionMatch& empty_decor = EMPTY_DECOR.match( ldd->decors );
        if ( !empty_decor.hasMatch() ) {
            main_color = mark_color;
        }
    }

    if ( ldd->rotations.size() == 1 ) {        // unflexible part
        out << ldr( main_color, lid, ldd->positions.first(), ldd->rotations.first(), x2l_tr, csub.substitute.transformation )
            << LDR_ENDL;
    } else {                                   // flexible part, not flexed
        out << QStringLiteral( "0 LXF2LDR BEGIN FLEXIBLE PART\r\n" );

        if ( !flexdb.isFlexible( ldd->id ) ) { // not known
            // first bone is the part
            out << ldr( main_color, lid, ldd->positions.first(), ldd->rotations.first(), x2l_tr, csub.substitute.transformation )
                << LDR_ENDL;
            // remaining bones are commented
            for ( int i = 1; i < ldd->positions.size(); ++i ) {
                out << ldr( main_color, QString(), ldd->positions.at( i ), ldd->rotations.at( i ), x2l_tr, csub.substitute.transformation, true )
                    << LDR_ENDL;
            }
        } else {                              // known flexible part
            const Flexible flex = flexdb.x2l_flex( ldd->id );

            // if there are two colors, use the second one for the ends
            // (only one flexible part does that)
            int color_ends = main_color;
            if ( colors.size() > 1 ) {
                color_ends = colors.at( 1 );
            }

            // supplemental head
            if ( flex.head_plus ) {
                out << ldr( color_ends, flex.head_plus->datfile, ldd->positions.first(), ldd->rotations.first(), Transformation::ID, flex.head_plus->transformation )
                    << LDR_ENDL;
            }

            // LSynth commands
            out << FMT_SYNTH.arg( flex.type ).arg( main_color );
            const int max_count = ldd->positions.size();
            // replacing head bones with (green) constraints
            if ( flex.head ) {
                for ( int i = 0; i < flex.head->count; ++i ) {
                    out << ldr( 2, SYNTH_PART, ldd->positions.at( i ), ldd->rotations.at( i ), Transformation::ID, flex.head->transformation )
                        << LDR_ENDL;
                }
            }
            // replacing body bones with constraints
            if ( flex.body ) {
                for ( int i = flex.body->before; i < max_count - flex.body->after; ++i ) {
                    out << ldr( main_color, SYNTH_PART, ldd->positions.at( i ), ldd->rotations.at( i ), Transformation::ID, flex.body->transformation )
                        << LDR_ENDL;
                }
            }
            // replacing tail bones with (red) constraints
            if ( flex.tail ) {
                for ( int i = max_count - flex.tail->count; i < max_count; ++i ) {
                    out << ldr( 4, SYNTH_PART, ldd->positions.at( i ), ldd->rotations.at( i ), Transformation::ID, flex.tail->transformation )
                        << LDR_ENDL;
                }
            }
            out << QStringLiteral( "0 SYNTH END\r\n" );

            // supplemental tail
            if ( flex.tail_plus ) {
                out << ldr( color_ends, flex.tail_plus->datfile, ldd->positions.last(), ldd->rotations.last(), Transformation::ID, flex.tail_plus->transformation )
                    << LDR_ENDL;
            }
        }
        out << QStringLiteral( "0 LXF2LDR END FLEXIBLE PART\r\n" );
    }
}

// helper function to output a group as a submodel
void group2ldr( const Group *const group, QTextStream& out, int mark_color, bool use_steps,
                const LDrawXML& ldrawxml, const Decors& decors, const Flex& flexdb )
{
    Q_ASSERT( group );

    out << QStringLiteral( "0 FILE %1\r\n" ).arg( group->name );
    for ( const Part *const part: group->parts ) {
        if ( use_steps ) {
            out << QStringLiteral( "0 STEP\r\n" );
        }
        part2ldr( part, out, mark_color, ldrawxml, decors, flexdb );
    }
    for ( const Group *const son: group->groups ) {
        if ( use_steps ) {
            out << QStringLiteral( "0 STEP\r\n" );
        }
        out << ldr( 16, son->name, son->translation, QMatrix4x4(), Transformation::ID, Transformation::ID )
            << LDR_ENDL;
    }
    out << LDR_ENDL;
}

// main function to translate and output a model
void lxf2ldr( const LXF& lxf, QTextStream& out,
              bool use_instructions, int mark_color,
              const LDrawXML& ldrawxml, const Decors& decors, const Flex& flexdb )
{
    bool use_steps = use_instructions && lxf.hasSteps();

    // output as group
    group2ldr( lxf.mainGroup(), out, mark_color, use_steps, ldrawxml, decors, flexdb );

    // output groups contents
    for ( QListIterator<Group> it = lxf.allGroups(); it.hasNext(); ) {
        group2ldr( &it.next(), out, mark_color, use_steps, ldrawxml, decors, flexdb );
    }

    // output assemblies contents
    for ( QListIterator<Group> it = lxf.allAssemblies(); it.hasNext(); ) {
        group2ldr( &it.next(), out, mark_color, use_steps, ldrawxml, decors, flexdb );
    }
    out << LDR_ENDL;
}
