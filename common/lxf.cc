/*
 *  Copyright 2017-2022, 2024  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lxf.h"

#include <quazip5/quazipfile.h>

void Part::recenter( const QVector3D& center )
{
    for ( int i = 0; i < positions.size(); ++i ) {
        positions[i] -= center;
    }
}

void Group::recenter( const QVector3D& center )
{
    // make everyone relative to me
    for ( Part *const part: parts ) {
        part->recenter( translation );
    }
    for ( Group* son: groups ) {
        son->recenter( translation );
    }
    // make me relative to daddy
    translation -= center;
}


LXF::LXF()
    : m_group( nullptr )
{}

LXF::~LXF()
{
    delete m_group;
}

const Group* LXF::mainGroup() const
{
    return m_group;
}

QListIterator<Group> LXF::allGroups() const
{
    return QListIterator<Group>( m_all_groups );
}

QListIterator<Group> LXF::allAssemblies() const
{
    return QListIterator<Group>( m_all_assemblies );
}

bool LXF::hasSteps() const
{
    return !m_steps.isEmpty();
}

QString LXF::stats() const
{
    static const QString FMT = QStringLiteral( "%1 parts / %2 groups and subgroups" );
    return FMT.arg( m_all_parts.size() ).arg( m_all_groups.size() );
}

const QString& LXF::errorString() const
{
    return m_error;
}

bool LXF::read( const QString& path, bool use_instructions )
{
    static const QString FMT_FLEX = QStringLiteral( "%1_flexible_%2" );

    m_name = path.section( '/', -1 ).replace( ' ', '_' ).replace( '.', '_' );

    // read the file
    QuaZipFile qzf( path, "IMAGE100.LXFML" );
    if ( qzf.getZipError() ) {
        m_error += QStringLiteral( "Opening “%1” failed." ).arg( path );
        m_error += QStringLiteral( "\nZip error %1" ).arg( qzf.getZipError() );
        return false;
    }
    if ( !qzf.open( QIODevice::ReadOnly ) ) {
        m_error += QStringLiteral( "Opening “IMAGE100.LXFML” failed." );
        m_error += QStringLiteral( "\nZip error %1" ).arg( qzf.getZipError() );
        return false;
    }
    m_xml.setDevice( &qzf );
    QList<Group*> groups0;
    if ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "LXFML" ) {
            if ( !readLXFML( groups0 ) ) {
                return false;
            }
        } else {
            m_error += QStringLiteral( "Expecting element LXFML, got “%1”." ).arg( m_xml.name().toString() );
            return false;
        }
    }
    qzf.close();

    if ( use_instructions && !m_steps.isEmpty() ) {
        // forget groups
        m_all_groups = QList<Group>();

        // transform steps into groups
        QList<Group*> groups;
        for ( const Step *const step: m_steps ) {
            step2Group( step, groups );
        }

        // create main group
        m_group = new Group( m_name, QList<Part*>(), groups, QVector3D() );

    } else {

        // put all parts not already in a group in the main group
        QList<Part*> parts;
        int flex = 0;
        for ( Part *const part: m_group_refs.values() ) {
            if ( part->positions.size() == 1 ) {
                parts << part;
            } else { // flexible part, putting it in its own group
                m_all_groups << Group( FMT_FLEX.arg( m_name ).arg( flex++ ),
                                       QList<Part*>() << part,
                                       QList<Group*>(),
                                       part->positions.first() );
                groups0 << &m_all_groups.last();
            }
        }

        // create main group
        m_group = new Group( m_name, parts, groups0, QVector3D() );
    }

    // remove assembly-subparts from their groups and put the assembly-group
    // instead
    for ( Group& group: m_all_groups ) {
        manageAssemblies( &group );
    }
    manageAssemblies( m_group );

    // recenter all groups on their position
    QVector3D position;
    if ( m_group->parts.isEmpty() ) {
        if ( m_group->groups.isEmpty() ) {
            // empty group
        } else {
            position = m_group->groups.first()->translation;
        }
    } else {
        position = m_group->parts.first()->positions.first();
    }
    m_group->recenter( position );

    return !m_xml.hasError();
}

void LXF::manageAssemblies( Group* const group ) {
    QList<Part*> to_remove;
    QList<Part*> to_keep;
    QList<Group*> assemblies;
    for ( Part* part: group->parts ) {
        if ( m_map_assemblies.contains( part->ref ) ) {
            to_remove << part;
            Group *const assembly = m_map_assemblies.value( part->ref );
            if ( assembly ) {
                assemblies << assembly;
                // we won’t add this assembly anymore but parts are still
                // to be removed, so refs aren’t removed
                for ( const Part *const subpart: assembly->parts ) {
                    m_map_assemblies.insert( subpart->ref, nullptr );
                }
            }
        } else {
            to_keep << part;
        }
    }

    if ( !to_remove.isEmpty() ) { // found some
        group->parts = to_keep;
        group->groups += assemblies;
    }
}

void LXF::step2Group( const Step *const step, QList<Group*>& siblings ) {
    static const QString FMT_FLEX = QStringLiteral( "%1_flexible_%2" );

    QList<Group*> sons;
    QList<Part*> parts;
    int flex = 0;
    for ( QString ref: step->refs ) {
        Part *const part = m_step_refs.take( ref );
        if ( part->positions.size() == 1 ) {
            parts << part;
        } else { // flexible part, putting it in its own group
            m_all_groups << Group( FMT_FLEX.arg( step->name ).arg( flex++ ),
                                   QList<Part*>() << part,
                                   QList<Group*>(),
                                   part->positions.first() );
            sons << &m_all_groups.last();
        }
    }
    for ( const Step *const son: step->steps ) {
        step2Group( son, sons );
    }

    QVector3D position;
    if ( parts.isEmpty() ) {
        if ( sons.isEmpty() ) {
            // empty group
        } else {
            position = sons.first()->translation;
        }
    } else {
        position = parts.first()->positions.first();
    }
    m_all_groups << Group( step->name, parts, sons, position );
    siblings << &m_all_groups.last();
}

bool LXF::readLXFML( QList<Group*>& groups )
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "LXFML" );

    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Bricks" ) {
            if ( !readBricks() ) {
                return false;
            }
        } else if ( m_xml.name() == "GroupSystems" ) {
            if ( !readGroupSystems( groups ) ) {
                return false;
            }
        } else if ( m_xml.name() == "BuildingInstructions" ) {
            if ( !readInstructions() ) {
                return false;
            }
        } else {
            m_xml.skipCurrentElement();
        }
    }
    return true;
}

bool LXF::readBricks()
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "Bricks" );

    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Brick" ) {
            if ( !readBrick() ) {
                return false;
            }
        } else {
            m_xml.skipCurrentElement();
        }
    }
    return true;
}

bool LXF::readBrick()
{
    static const QString FMT_ASSEMBLY = QStringLiteral( "%1_assembly_%2" );

    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "Brick" );

    const int expected_next_index = m_all_parts.size();
    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Part" ) {
            if ( !readPart() ) {
                return false;
            }
        } else {
            m_xml.skipCurrentElement();
        }
    }
    const int last_index = m_all_parts.size() - 1;
    if ( last_index > expected_next_index  ) { // more than one part read => assembly
        // create a group/assembly
        QList<Part*> read_parts;
        for ( int i = expected_next_index; i < m_all_parts.size(); ++i ) {
            read_parts << &m_all_parts[ i ];
        }
        m_all_assemblies << Group( FMT_ASSEMBLY.arg( m_name ).arg( m_all_assemblies.size() ),
                                   read_parts, QList<Group*>(),
                                   read_parts.first()->positions.first() );
        // reference the assembly
        Group *const assembly = &m_all_assemblies.last();
        for ( Part *const part: assembly->parts ) {
            m_map_assemblies.insert( part->ref, assembly );
        }
    }
    return true;
}

bool LXF::readPart()
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "Part" );

    bool ok;
    const QString sid = m_xml.attributes().value( QStringLiteral( "designID" ) )
        .toString().split( ';' )[0]; // LXF v8
    const int id = sid.toInt( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Part: designID attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "designID" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }

    const QString decors = m_xml.attributes().value( QStringLiteral( "decoration" ) ).toString();

    const QStringList colors_txt = m_xml.attributes().value( QStringLiteral( "materials" ) ).toString().split( ',' );
    QList<int> colors;
    for ( const QString& color_txt: colors_txt ) {
        const QString color_txt_i = color_txt.split( ':' )[0]; // v8
        int color = color_txt_i.toInt( &ok );
        if ( !ok ) {
            m_error += QStringLiteral( "Part: materials attribute, color isn’t a number “%1”." ).arg( color_txt );
            m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
            return false;
        }
        colors << color;
    }

    QString ref = m_xml.attributes().value( QStringLiteral( "refID" ) ).toString();
    if ( ref.isEmpty() ) { // v8
        ref = m_xml.attributes().value( QStringLiteral( "uuid" ) ).toString();
    }
    QStringList nodes_transf_txt;
    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Bone" ) {
            nodes_transf_txt << m_xml.attributes().value( QStringLiteral( "transformation" ) ).toString();
            m_xml.skipCurrentElement();
        } else {
            m_xml.skipCurrentElement();
        }
    }
    if ( nodes_transf_txt.size() < 1 ) {
        m_error += QStringLiteral( "Part: no nodes." );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    QList<QVector3D> positions;
    QList<QMatrix4x4> rotations;
    for ( const QString& transf_txt: nodes_transf_txt ) {
        const QStringList mat_txt = transf_txt.split( ',' );
        if ( mat_txt.size() != 12 ) {
            m_error += QStringLiteral( "Part: Bone: transformation hasn’t 12 numbers “%1”." ).arg( transf_txt );
            m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
            return false;
        }
        QList<double> mat_real;
        for ( int i = 0; i < mat_txt.size(); ++i ) {
            bool ok;
            double r = mat_txt.at( i ).toDouble( &ok );
            if ( !ok ) {
                m_error += QStringLiteral( "Part: Bone: transformation: value #%1 isn’t a number “%2”." ).arg( i ).arg( mat_txt.at( i ) );
                m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
                return false;
            }
            mat_real << r;
        }
        positions << QVector3D( mat_real.at( 9 ), mat_real.at( 10 ), mat_real.at( 11 ) );
        QMatrix4x4 rot(mat_real.at( 0 ), mat_real.at( 3 ), mat_real.at( 6 ), 0,
                       mat_real.at( 1 ), mat_real.at( 4 ), mat_real.at( 7 ), 0,
                       mat_real.at( 2 ), mat_real.at( 5 ), mat_real.at( 8 ), 0,
                       0,                0,                0,                1 );
        rot.optimize();
        rotations << rot;
    }

    m_all_parts << Part( ref, id, colors, decors, positions, rotations );
    m_group_refs.insert( ref, &m_all_parts.last() );
    m_step_refs.insert( ref, &m_all_parts.last() );
    return true;
}

bool LXF::readGroupSystems( QList<Group*>& groups )
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "GroupSystems" );

    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "GroupSystem" ) {
            if ( !readGroupSystem( groups ) ) {
                return false;
            }
        } else {
            m_xml.skipCurrentElement();
        }
    }
    return true;
}

bool LXF::readGroupSystem( QList<Group*>& groups )
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "GroupSystem" );

    int i = 0;
    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Group" ) {
            if ( !readGroup( m_name, i++, groups ) ) {
                return false;
            }
        } else {
            m_xml.skipCurrentElement();
        }
    }
    return true;
}

bool LXF::readGroup( const QString& base_name, int number, QList<Group*>& siblings )
{
    static const QString FMT_NAME = QStringLiteral( "%1_%2" );
    static const QString FMT_FLEX = QStringLiteral( "%1_flexible_%2" );

    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "Group" );

    const QString name = FMT_NAME.arg( base_name ).arg( number );
    QList<Group*> sons;
    QList<Part*> parts;
    int flex = 0;

    const QString srefs = m_xml.attributes().value( QStringLiteral( "partRefs" ) ).toString();
    if ( !srefs.isEmpty() ) {
        const QStringList refs = srefs.split( ',' );
        for ( QString ref: refs ) {
            if ( !m_group_refs.contains( ref ) ) {
                m_error += QStringLiteral( "Group: partRefs list element isn’t a known refID “%1”." ).arg( ref );
                m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
                return false;
            }
            Part *const part = m_group_refs.take( ref );
            if ( part->positions.size() == 1 ) {
                parts << part;
            } else { // flexible part, putting it in its own group
                m_all_groups << Group( FMT_FLEX.arg( name ).arg( flex++ ),
                                       QList<Part*>() << part,
                                       QList<Group*>(),
                                       part->positions.first() );
                sons << &m_all_groups.last();
            }
        }
    }

    int i = 0;
    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Group" ) {
            if ( !readGroup( name, i++, sons ) ) {
                return false;
            }
        } else {
            m_xml.skipCurrentElement();
        }
    }
    QVector3D position;
    if ( parts.isEmpty() ) {
        if ( sons.isEmpty() ) {
            // empty group
        } else {
            position = sons.first()->translation;
        }
    } else {
        position = parts.first()->positions.first();
    }
    m_all_groups << Group( name, parts, sons, position );
    siblings << &m_all_groups.last();

    return true;
}

bool LXF::readInstructions()
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "BuildingInstructions" );

    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "BuildingInstruction" ) {
            if ( !readInstruction() ) {
                return false;
            }
        } else {
            m_xml.skipCurrentElement();
        }
    }
    return true;
}

bool LXF::readInstruction()
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "BuildingInstruction" );

    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Step" ) {
            if ( !readStep( m_steps ) ) {
                return false;
            }
        } else {
            m_xml.skipCurrentElement();
        }
    }
    return true;
}

bool LXF::readPartRef( QStringList& refs )
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "PartRef" );

    const QString ref = m_xml.attributes().value( QStringLiteral( "partRef" ) ).toString();
    if ( !m_map_assemblies.contains( ref ) && !m_step_refs.contains( ref ) ) {
        m_error += QStringLiteral( "Step: partRef list element isn’t a known refID “%1”." ).arg( ref );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    refs << ref;
    return true;
}

bool LXF::readStep( QList<Step*>& siblings )
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "Step" );

    const QString name = m_xml.attributes().value( QStringLiteral( "name" ) ).toString();
    QList<Step*> sons;
    QStringList refs;

    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Step" ) {
            if ( !readStep( sons ) ) {
                return false;
            }
        } else if ( m_xml.name() == "PartRef" ) {
            if ( !readPartRef( refs ) ) {
                return false;
            }
            m_xml.skipCurrentElement();
        } else {
            m_xml.skipCurrentElement();
        }
    }
    m_all_steps << Step( name, refs, sons );
    siblings << &m_all_steps.last();
    return true;
}
