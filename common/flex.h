/*
 *  Copyright 2017, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FLEX_H__
#define __FLEX_H__

#include <QMap>
#include <QList>
#include <optional>

#include "substitute.h"
#include "transformation.h"

//
struct Counted
{
    int count;
    std::optional<Transformation> transformation;

    explicit Counted()
        : count( 0 ), transformation( Transformation::ID )
        {}
    explicit Counted( int vcount, std::optional<Transformation> vtransformation )
        : count( vcount ), transformation( vtransformation )
        {}
};

//
struct BiCounted
{
    int before;
    int after;
    std::optional<Transformation> transformation;

    explicit BiCounted()
        : before( 0 ), after( 0 ), transformation( Transformation::ID )
        {}
    explicit BiCounted( int vbefore, int vafter, const std::optional<Transformation>& vtransformation )
        : before( vbefore ), after( vafter ), transformation( vtransformation )
        {}
};


// components of a flexible part
struct Flexible
{
    QString type;
    std::optional<Counted> head;
    std::optional<BiCounted> body;
    std::optional<Counted> tail;
    std::optional<Substitute> head_plus;
    std::optional<Substitute> tail_plus;

    explicit Flexible()
        : type(), head(), body(), tail(), head_plus(), tail_plus()
        {}
    explicit Flexible( const QString& vtype,
                       std::optional<Counted> vhead,
                       std::optional<BiCounted> vbody,
                       std::optional<Counted> vtail,
                       std::optional<Substitute> vhead_plus,
                       std::optional<Substitute> vtail_plus )
        : type( vtype ), head( vhead ), body( vbody ), tail( vtail ),
          head_plus( vhead_plus ), tail_plus( vtail_plus )
        {}
};


// LDD (id) to LDR (files + transformations) database
class Flex
{
public:
    explicit Flex() {}

    bool read( const QString& path );
    QString stats() const;
    const QString& errorString() const;

    bool isFlexible( int id ) const;
    const Flexible x2l_flex( int id ) const;

protected:
    std::optional<double>         readDouble( const QString& txt, const QString& name );
    std::optional<Transformation> readTransformation( const QString& txt );
    std::optional<Substitute>     readSubstitute( const QString& txt );
    std::optional<Counted>        readCounted( const QString& txt );
    std::optional<BiCounted>      readBiCounted( const QString& txt );

private:
    QMap<int, Flexible> m_flex;
    QString m_error;
};

#endif
