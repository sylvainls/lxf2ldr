/*
 *  Copyright 2017, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __DECORS_H__
#define __DECORS_H__

#include <QList>
#include <QMap>
#include <optional>

#include "substitute.h"

struct SimpleSubstitute
{
    QString datfile;
    bool overwrite;

    explicit SimpleSubstitute()
        : datfile(), overwrite( false )
        {}
    explicit SimpleSubstitute( const QString& vdatfile, bool voverwrite )
        : datfile( vdatfile ), overwrite( voverwrite )
        {}
};

struct ColorSubstitute {
    int usecolor;
    Substitute substitute;

    explicit ColorSubstitute()
        : usecolor( 0 ), substitute()
        {}
    explicit ColorSubstitute( int vusecolor, const Substitute& vsubstitute )
        : usecolor( vusecolor ), substitute( vsubstitute )
        {}
};

typedef QMap<int, SimpleSubstitute> ColorMap;
typedef QMap<QString, Substitute> DecorationMap;

struct DecorMatch
{
    int usecolor;
    QList<ColorMap> colors;
    DecorationMap decorations;

    explicit DecorMatch()
        : usecolor( 0 ), colors(), decorations()
        {}
    explicit DecorMatch( int vusecolor, const QList<ColorMap>& vcolors, const DecorationMap& vdecorations )
        : usecolor( vusecolor ), colors( vcolors ), decorations( vdecorations )
        {}
};

namespace YAML {
    class Node;
}

// LDD (id + decorations + colors) to LDR database
class Decors
{
public:
    explicit Decors() {}

    bool read( const QString& path );
    QString stats() const;
    const QString& errorString() const;

    ColorSubstitute x2l_decor( int lego, const QString& decors, const QList<int>& colors ) const;

protected:
    // helper functions
    std::optional<DecorMatch>      readDecorMatch( const YAML::Node& yml );
    std::optional<DecorationMap>   readDecorations( const YAML::Node& yml );
    std::optional<QList<ColorMap>> readColors( const YAML::Node& yml );

private:
    QMap<int, DecorMatch> m_decors;
    QString m_error;
};

#endif
