INCLUDEPATH += . ..
QT += xml core
LIBS += -lquazip5 -lyaml-cpp
HEADERS = ../common/lxf.h ../common/math_utils.h ../common/transformation.h ../common/ldrawxml.h ../common/substitute.h ../common/decors.h ../common/flex.h ../common/lxf2ldr.h
SOURCES = ../common/lxf.cc ../common/math_utils.cc ../common/ldrawxml.cc ../common/decors.cc ../common/flex.cc ../common/lxf2ldr.cc
RESOURCES = ../common/resources.qrc
