/*
 *  Copyright 2017, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "flex.h"

#include <QFile>
#include <QTextStream>
#include <yaml-cpp/yaml.h>
#include "math_utils.h"
#include "transformation.h"

const QString& Flex::errorString() const
{
    return m_error;
}

std::optional<double> Flex::readDouble( const QString& txt, const QString& name )
{
    bool ok;
    const double x = txt.toDouble( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "%1 isn’t a number: “%2”" ).arg( name ).arg( txt );
        return {};
    }
    return x;
}

std::optional<Transformation> Flex::readTransformation( const QString& txt )
{
    if ( txt.isEmpty() ) {
        return Transformation::ID;
    }
    const QStringList& transtxt = txt.split( ',' );
    if ( transtxt.size() != 7 ) {
        m_error += QStringLiteral( "“%1” is not a transformation: should have 7 comma-separated numbers (tx,ty,tz,ax,ay,az,angle)." ).arg( txt );
        return {};
    }
    std::optional<double> tx = readDouble( transtxt.at( 0 ), QStringLiteral( "tx" ) );
    std::optional<double> ty = readDouble( transtxt.at( 1 ), QStringLiteral( "ty" ) );
    std::optional<double> tz = readDouble( transtxt.at( 2 ), QStringLiteral( "tz" ) );
    std::optional<double> ax = readDouble( transtxt.at( 3 ), QStringLiteral( "ax" ) );
    std::optional<double> ay = readDouble( transtxt.at( 4 ), QStringLiteral( "ay" ) );
    std::optional<double> az = readDouble( transtxt.at( 5 ), QStringLiteral( "az" ) );
    std::optional<double> angle = readDouble( transtxt.at( 6 ), QStringLiteral( "angle" ) );
    if ( !tx.has_value() || !ty.has_value() || !tz.has_value() ||
         !ax.has_value() || !ay.has_value() || !az.has_value() ||
         !angle.has_value() ) {
        return {};
    }
    return Transformation( QVector3D( tx.value(), ty.value(), tz.value() ),
                           rot2mat( ax.value(), ay.value(), az.value(), angle.value() ) );
}

std::optional<Substitute> Flex::readSubstitute( const QString& txt )
{
    const QStringList& txt_l = txt.split( ' ' );
    const QString& datfile = txt_l.first().toLower();
    std::optional<Transformation> transformation;
    if ( txt_l.size() > 1 ) {
        const QString& transtxt = txt_l.last();
        transformation = readTransformation( transtxt );
        if ( !transformation ) {
            m_error += QStringLiteral( "\n“%1” is not a transformation (for “%2”)" ).arg( transtxt ).arg( datfile );
            return {};
        }
    }
    return Substitute( datfile, transformation );
}

std::optional<Counted> Flex::readCounted( const QString& txt )
{
    const QStringList& txt_l = txt.split( ' ' );
    bool ok;
    const int count = txt_l.first().toInt( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Count isn’t a number: “%1”" ).arg( txt_l.first() );
        return {};
    }
    std::optional<Transformation> transformation;
    if ( txt_l.size() > 1 ) {
        const QString& transtxt = txt_l.last();
        transformation = readTransformation( transtxt );
        if ( !transformation ) {
            m_error += QStringLiteral( "\n“%1” is not a transformation." ).arg( transtxt );
            return {};
        }
    }
    return Counted( count, transformation );
}

std::optional<BiCounted> Flex::readBiCounted( const QString& txt )
{
    const QStringList& txt_l = txt.split( ' ' );
    const QStringList& counts = txt_l.first().split( ',' );
    bool ok;
    const int before = counts.first().toInt( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "First count isn’t a number: “%1”" ).arg( counts.first() );
        return {};
    }
    const int after = counts.last().toInt( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Last count isn’t a number: “%1”" ).arg( counts.last() );
        return {};
    }
    std::optional<Transformation> transformation;
    if ( txt_l.size() > 1 ) {
        const QString& transtxt = txt_l.last();
        transformation = readTransformation( transtxt );
        if ( !transformation ) {
            m_error += QStringLiteral( "\n“%1” is not a transformation." ).arg( transtxt );
            return {};
        }
    }
    return BiCounted( before, after, transformation );
}

bool Flex::read( const QString& path )
{
    QFile file( path );
    if ( !file.open( QFile::ReadOnly | QFile::Text ) ) {
        m_error += QStringLiteral( "Open “%1” failed.\n" ).arg( path );
        m_error += file.errorString();
        return false;
    }
    QTextStream stream( &file );

    const YAML::Node yml = YAML::Load( stream.readAll().toStdString() );
    if ( yml.IsNull() ) { // accepts empty files
        return true;
    }

    if ( !yml.IsMap() ) {
        m_error += QStringLiteral( "The database is not a map." );
        m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( yml.Mark().line ).arg( yml.Mark().column ).arg( yml.Mark().pos );
        return false;
    }

    const YAML::const_iterator& yml_end = yml.end();
    for ( YAML::const_iterator it = yml.begin(); it != yml_end; ++it ) {
        if ( !it->first.IsScalar() ) {
            m_error += QStringLiteral( "Key is not a scalar." );
            m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( it->first.Mark().line ).arg( it->first.Mark().column ).arg( it->first.Mark().pos );
            return false;
        }
        const int lego = it->first.as<int>();
        const YAML::Node subyml = it->second;
        if ( !subyml.IsMap() ) {
            m_error += QStringLiteral( "Value is not a map." );
            m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( subyml.Mark().line ).arg( subyml.Mark().column ).arg( subyml.Mark().pos );
            return false;
        }
        QString type;
        std::optional<Counted> head;
        std::optional<BiCounted> body;
        std::optional<Counted> tail;
        std::optional<Substitute> head_plus;
        std::optional<Substitute> tail_plus;
        YAML::const_iterator subyml_end = subyml.end();
        for ( YAML::const_iterator jt = subyml.begin(); jt != subyml_end; ++jt ) {
            if ( !jt->first.IsScalar() ) {
                m_error += QStringLiteral( "Key is not a scalar." );
                m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( jt->first.Mark().line ).arg( jt->first.Mark().column ).arg( jt->first.Mark().pos );
                return false;
            }
            const QString& key = QString::fromStdString( jt->first.as<std::string>() );
            if ( !jt->second.IsScalar() ) {
                m_error += QStringLiteral( "Value is not a scalar." );
                m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( jt->second.Mark().line ).arg( jt->second.Mark().column ).arg( jt->second.Mark().pos );
                return false;
            }
            const QString value = QString::fromStdString( jt->second.as<std::string>() );
            if ( key == QStringLiteral( "type" ) ) {
                type = value;
            } else if ( key == QStringLiteral( "body" ) ) {
                body = readBiCounted( value );
                if ( !body ) {
                    m_error += QStringLiteral( "\nCouldn’t read value for body." );
                    m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( jt->second.Mark().line ).arg( jt->second.Mark().column ).arg( jt->second.Mark().pos );
                    return false;
                }
            } else if ( key == QStringLiteral( "head" ) ) {
                head = readCounted( value );
                if ( !head ) {
                    m_error += QStringLiteral( "\nCouldn’t read value for head." );
                    m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( jt->second.Mark().line ).arg( jt->second.Mark().column ).arg( jt->second.Mark().pos );
                    return false;
                }
            } else if ( key == QStringLiteral( "tail" ) ) {
                tail = readCounted( value );
                if ( !tail ) {
                    m_error += QStringLiteral( "\nCouldn’t read value for tail." );
                    m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( jt->second.Mark().line ).arg( jt->second.Mark().column ).arg( jt->second.Mark().pos );
                    return false;
                }
            } else if ( key == QStringLiteral( "head+" ) ) {
                head_plus = readSubstitute( value );
                if ( !head_plus ) {
                    m_error += QStringLiteral( "\nCouldn’t read value for head+." );
                    m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( jt->second.Mark().line ).arg( jt->second.Mark().column ).arg( jt->second.Mark().pos );
                    return false;
                }
            } else if ( key == QStringLiteral( "tail+" ) ) {
                tail_plus = readSubstitute( value );
                if ( !tail_plus ) {
                    m_error += QStringLiteral( "\nCouldn’t read value for tail+." );
                    m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( jt->second.Mark().line ).arg( jt->second.Mark().column ).arg( jt->second.Mark().pos );
                    return false;
                }
            }
        }
        m_flex.insert( lego, Flexible( type, head, body, tail, head_plus, tail_plus ) );
    }
    return true;
}

QString Flex::stats() const
{
    const static QString FMT = QStringLiteral( "%1 flexible parts" );
    return FMT.arg( m_flex.size() );
}

bool Flex::isFlexible( int id ) const
{
    return m_flex.contains( id );
}

const Flexible Flex::x2l_flex( int id ) const
{
    return m_flex.value( id );
}
