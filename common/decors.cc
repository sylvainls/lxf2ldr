/*
 *  Copyright 2017, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "decors.h"

#include <QFile>
#include <QTextStream>
#include <QRegularExpression>
#include <yaml-cpp/yaml.h>
#include "transformation.h"
#include "substitute.h"
#include "math_utils.h"

const QString& Decors::errorString() const
{
    return m_error;
}

// helper function to read a list of matches by color
std::optional<QList<ColorMap>> Decors::readColors( const YAML::Node& yml )
{
    if ( !yml.IsSequence() ) {
        m_error += QStringLiteral( "Value is not a sequence." );
        m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( yml.Mark().line ).arg( yml.Mark().column ).arg( yml.Mark().pos );
        return {};
    }
    QList<ColorMap> colormaps;
    int i = 0;
    const YAML::const_iterator& yml_end = yml.end();
    for ( YAML::const_iterator it = yml.begin(); it != yml_end; ++it, ++i ) {
        if ( it->IsNull() ) {
            colormaps << ColorMap();
            continue;
        }
        if ( !it->IsMap() ) {
            m_error += QStringLiteral( "Value for color #%1 is not a map." ).arg( i );
            m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( it->Mark().line ).arg( it->Mark().column ).arg( it->Mark().pos );
            return {};
        }
        const YAML::Node& subyml = *it;
        ColorMap colormap;
        const YAML::const_iterator& subyml_end = subyml.end();
        for ( YAML::const_iterator jt = subyml.begin(); jt != subyml_end; ++jt ) {
            if ( !jt->first.IsScalar() ) {
                m_error += QStringLiteral( "Key is not a scalar." );
                m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( jt->first.Mark().line ).arg( jt->first.Mark().column ).arg( jt->first.Mark().pos );
                return {};
            }
            const int ldd_color = jt->first.as<int>();
            const YAML::Node valueyml = jt->second;
            if ( !valueyml.IsScalar() ) {
                m_error += QStringLiteral( "Value is not a scalar." );
                m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( valueyml.Mark().line ).arg( valueyml.Mark().column ).arg( valueyml.Mark().pos );
                return {};
            }
            const QStringList& simplelst = QString::fromStdString( valueyml.as<std::string>() ).split( ' ' );
            const QString& datfile = simplelst.first().toLower();
            bool overwrite = false;
            if ( simplelst.size() > 1 ) {
                overwrite = simplelst.at( 1 ).toUpper() == QStringLiteral( "OW" );
            }
            colormap.insert( ldd_color, SimpleSubstitute( datfile, overwrite ) );
        }
        colormaps << colormap;
    }
    return colormaps;
}

// helper function to read decorations matches
std::optional<DecorationMap> Decors::readDecorations( const YAML::Node& yml )
{
    // simple matrices as expressed in the decors database
    const static QMap<QString, QMatrix4x4> LOCAL_ROT = {
        { "x",     rot2mat( 1, 0, 0,   90 ) },
        { "xx",    rot2mat( 1, 0, 0,  180 ) },
        { "xxx",   rot2mat( 1, 0, 0,  -90 ) },
        { "y",     rot2mat( 0, 1, 0,   90 ) },
        { "yy",    rot2mat( 0, 1, 0,  180 ) },
        { "yyy",   rot2mat( 0, 1, 0,  -90 ) },
        { "z",     rot2mat( 0, 0, 1,   90 ) },
        { "zz",    rot2mat( 0, 0, 1,  180 ) },
        { "zzz",   rot2mat( 0, 0, 1,  -90 ) },
    };
    const static QRegularExpression SPECIAL_ROT( "\\A([xyz])(\\d+)\\Z" );

    if ( !yml.IsMap() ) {
        m_error += QStringLiteral( "Value is not a map." );
        m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( yml.Mark().line ).arg( yml.Mark().column ).arg( yml.Mark().pos );
        return {};
    }
    DecorationMap decorations;
    const YAML::const_iterator& yml_end = yml.end();
    for( YAML::const_iterator it = yml.begin(); it != yml_end; ++it ) {
        if ( !it->first.IsScalar() ) {
            m_error += QStringLiteral( "Key is not a scalar." );
            m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( it->first.Mark().line ).arg( it->first.Mark().column ).arg( it->first.Mark().pos );
            return {};
        }

        const QString& key = QString::fromStdString( it->first.as<std::string>() );
        const YAML::Node subyml = it->second;
        if ( !subyml.IsScalar() ) {
            m_error += QStringLiteral( "Value is not a scalar." );
            m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( subyml.Mark().line ).arg( subyml.Mark().column ).arg( subyml.Mark().pos );
            return {};
        }

        const QStringList& sublst = QString::fromStdString( subyml.as<std::string>() ).split( ' ' );
        const QString& datfile = sublst.first().toLower();
        QString rotation;
        if ( sublst.size() > 1 ) {
            rotation = sublst.at( 1 );
        }
        if ( LOCAL_ROT.contains( rotation ) ) {
            decorations.insert( key, Substitute( datfile, Transformation( QVector3D(), LOCAL_ROT[rotation] ) ) );
        } else {
            const QRegularExpressionMatch& m = SPECIAL_ROT.match( rotation );
            if ( m.hasMatch() ) {
                const QString& axis = m.captured(1);
                bool ok = false;
                const int angle = m.captured(2).toInt( &ok );
                // RE tells us ok will always be true

                QMatrix4x4 mat_rot;
                if ( "x" == axis ) {
                    mat_rot = rot2mat( 1, 0, 0, angle );
                } else if ( "y" == axis ) {
                    mat_rot = rot2mat( 0, 1, 0, angle );
                } else if ( "z" == axis ) {
                    mat_rot = rot2mat( 0, 0, 1, angle );
                }
                // RE tells us there are no other possibilities

                decorations.insert( key, Substitute( datfile, Transformation( QVector3D(), mat_rot ) ) );
            } else {
                decorations.insert( key, Substitute( datfile, Transformation::ID ) );
            }
        }
    }
    return decorations;
}

// helper function to read matches for a part
std::optional<DecorMatch> Decors::readDecorMatch( const YAML::Node& yml )
{
    if ( !yml.IsMap() ) {
        m_error += QStringLiteral( "Value is not a map." );
        m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( yml.Mark().line ).arg( yml.Mark().column ).arg( yml.Mark().pos );
        return {};
    }

    int usecolor = 0;
    std::optional<QList<ColorMap>> colors = QList<ColorMap>();
    std::optional<DecorationMap> decorations = DecorationMap();
    const YAML::const_iterator& yml_end = yml.end();
    for ( YAML::const_iterator it = yml.begin(); it != yml_end; ++it ) {
        if ( !it->first.IsScalar() ) {
            m_error += QStringLiteral( "Key is not a scalar." );
            m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( it->first.Mark().line ).arg( it->first.Mark().column ).arg( it->first.Mark().pos );
            return {};
        }

        const QString& key = QString::fromStdString( it->first.as<std::string>() );
        const YAML::Node subyml = it->second;
        if ( key == QStringLiteral( "usecolor" ) ) {
            if ( !subyml.IsScalar() ) {
                m_error += QStringLiteral( "usecolor is not a scalar." );
                m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( subyml.Mark().line ).arg( subyml.Mark().column ).arg( subyml.Mark().pos );
                return {};
            }
            usecolor = subyml.as<int>();
        } else if ( key == QStringLiteral( "colors" ) ) {
            colors = readColors( subyml );
            if ( !colors ) {
                m_error += QStringLiteral( "\nCouldn’t read color matches." );
                return {};
            }
        } else if ( key == QStringLiteral( "decorations" ) ) {
            decorations = readDecorations( subyml );
            if ( !decorations ) {
                m_error += QStringLiteral( "\nCouldn’t read decorations matches." );
                return {};
            }
        }
    }
    return DecorMatch( usecolor, colors.value(), decorations.value() );
}

bool Decors::read( const QString& path )
{
    QFile file( path );
    if ( !file.open( QFile::ReadOnly | QFile::Text ) ) {
        m_error += QStringLiteral( "Opening “%1” failed.\n" ).arg( path );
        m_error += file.errorString();
        return false;
    }

    QTextStream stream( &file );
    const YAML::Node yml = YAML::Load( stream.readAll().toStdString() );

    if ( yml.IsNull() ) { // accepts empty files
        return true;
    }
    if ( !yml.IsMap() ) {
        m_error += QStringLiteral( "The database is not a map." );
        return false;
    }

    const YAML::const_iterator& yml_end = yml.end();
    for ( YAML::const_iterator it = yml.begin(); it != yml_end; ++it ) {
        if ( !it->first.IsScalar() ) {
            m_error += QStringLiteral( "Key is not a scalar." );
            m_error += QStringLiteral( "\nLine %1 column %2 pos %3" ).arg( it->first.Mark().line ).arg( it->first.Mark().column ).arg( it->first.Mark().pos );
            return false;
        }
        std::optional<DecorMatch> decor = readDecorMatch( it->second );
        if ( !decor ) {
            m_error += QStringLiteral( "\nCouldn’t read decor entry." );
            return false;
        }
        m_decors.insert( it->first.as<int>(), decor.value() );
    }
    return true;
}

QString Decors::stats() const
{
    const static QString FMT = QStringLiteral( "%1 parts, %2 decorations" );
    int n = 0;
    for ( const DecorMatch& decor: m_decors ) {
        n += decor.decorations.size() + decor.colors.size();
    }
    return FMT.arg( m_decors.size() ).arg( n );
}

ColorSubstitute Decors::x2l_decor( int lego, const QString& decorations,
                                   const QList<int>& colors ) const
{
    if ( !m_decors.contains( lego ) ) {
        return ColorSubstitute();
    }
    const DecorMatch& decor = m_decors.value( lego );
    const int maxcol = qMin( colors.size(), decor.colors.size() );
    Substitute substitute;
    for ( int i = 0; i < maxcol; ++i ) {
        const int curcol = colors.at( i ) == 0 ? colors.first() : colors.at( i );
        if ( decor.colors.at( i ).contains( curcol ) ) {
            const SimpleSubstitute& sub = decor.colors.at( i ).value( curcol );
            if ( substitute.datfile.isEmpty() || sub.overwrite ) {
                substitute = Substitute( sub.datfile, Transformation::ID );
            }
        }
    }
    if ( decor.decorations.contains( decorations ) ) {
        substitute = decor.decorations.value( decorations );
    }
    return ColorSubstitute( decor.usecolor, substitute );
}
