/*
 *  Copyright 2017, 2020  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LXF2LDR_H__
#define __LXF2LDR_H__

class QTextStream;
class LXF;
class LDrawXML;
class Decors;
class Flex;

// outputs the LDR model converted from an LXF model, using conversion
// databases
void lxf2ldr( const LXF& lxf, QTextStream& out,
              bool use_instructions, int mark_color,
              const LDrawXML& ldrawxml, const Decors& decors, const Flex& flex );

#endif
