/*
 *  Copyright 2017, 2020, 2022, 2024  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LXF_H__
#define __LXF_H__

#include <QtXml>
#include <QList>
#include <QMap>
#include <QVector3D>
#include <QMatrix4x4>

// a LEGO brick placed in an LXF model
struct Part
{
    const QString ref;
    const int id;
    const QList<int> colors;
    const QString decors;
    QList<QVector3D> positions;
    QList<QMatrix4x4> rotations;

    explicit Part()
        : ref(), id(), colors(), decors(), positions(), rotations()
        {}
    explicit Part( const QString& vref, int vid, const QList<int>& vcolors,
                   const QString& vdecors, const QList<QVector3D>& vpos,
                   const QList<QMatrix4x4>& vrot )
        : ref( vref ), id( vid ), colors( vcolors ), decors( vdecors ),
          positions( vpos ), rotations( vrot )
        {}

    // reset positions relatively to center
    void recenter( const QVector3D& center );
};

// part groups
struct Group
{
    const QString name;
    QList<Part*> parts;
    QList<Group*> groups;
    QVector3D translation;

    explicit Group()
        : name(), parts(), groups(), translation()
        {}
    explicit Group( const QString& vname, const QList<Part*>& vparts,
                    const QList<Group*>& vgroups, const QVector3D& vtranslation )
        : name( vname ), parts( vparts ), groups( vgroups ), translation( vtranslation )
        {}

    // reset positions relatively to center
    void recenter( const QVector3D& center );
};

// instruction steps
struct Step
{
    const QString name;
    QStringList   refs;
    QList<Step*> steps;

    explicit Step()
        : name(), refs(), steps()
        {}
    explicit Step( const QString& vname, const QStringList& vrefs,
                   const QList<Step*>& vsteps )
        : name( vname ), refs( vrefs ), steps( vsteps )
        {}
};

// an LDD model
class LXF
{
public:
    explicit LXF();
    ~LXF();

    bool read( const QString& path, bool use_intructions );
    QString stats() const;
    const QString& errorString() const;

    const Group* mainGroup() const;
    QListIterator<Group> allGroups() const;
    QListIterator<Group> allAssemblies() const;
    bool hasSteps() const;

protected:
    bool readLXFML( QList<Group*>& siblings );
    bool readBricks();
    bool readBrick();
    bool readPart();
    bool readGroupSystems( QList<Group*>& siblings );
    bool readGroupSystem( QList<Group*>& siblings );
    bool readGroup( const QString& base_name, int number, QList<Group*>& siblings );
    void manageAssemblies( Group* const group );
    bool readInstructions();
    bool readInstruction();
    bool readPartRef( QStringList& siblings );
    bool readStep( QList<Step*>& siblings );
    void step2Group( const Step *const step, QList<Group*>& siblings );
    void steps2Groups();

private:
    // temporary reading data
    QXmlStreamReader m_xml;
    QMap<QString, Part*> m_group_refs;
    QMap<QString, Part*> m_step_refs;
    QMap<QString, Group*> m_map_assemblies;
    QString m_name;
    QString m_error;

    // actual data
    QList<Group> m_all_assemblies;
    QList<Part> m_all_parts;
    QList<Group> m_all_groups;
    QList<Step> m_all_steps;
    QList<Step*> m_steps;
    Group* m_group;
};

#endif
