/*
 *  Copyright 2017, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TRANSFORMATION_H__
#define __TRANSFORMATION_H__

#include <QMatrix4x4>
#include <QVector3D>
#include <optional>

// translation and rotation are separated because they are not applied
// together
struct Transformation
{
    QVector3D translation;
    QMatrix4x4 rotation;

    explicit Transformation()
        : translation(), rotation()
        {}
    explicit Transformation( const QVector3D& vtrans, const QMatrix4x4& vrot )
        : translation( vtrans ), rotation( vrot )
        {}

    static const std::nullopt_t ID; // empty/identity Transformation
};

#endif
