/*
 *  Copyright 2017, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SUBSTITUTE_H__
#define __SUBSTITUTE_H__

#include <QString>
#include <optional>

#include "transformation.h"

//
struct Substitute
{
    QString datfile;
    std::optional<Transformation> transformation;

    explicit Substitute()
        : datfile(), transformation( Transformation::ID )
        {}
    explicit Substitute( const QString& vdatfile,
                         std::optional<Transformation> vtransformation )
        : datfile( vdatfile ), transformation( vtransformation )
        {}
};

#endif
