/*
 *  Copyright 2017, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LDRAWXML_H__
#define __LDRAWXML_H__

#include <QtXml>
#include <QMap>
#include <QStringList>

#include "transformation.h"

// list of parts
struct Assembly
{
    QString id;
    QStringList parts;

    explicit Assembly()
        : id(), parts()
        {}
    explicit Assembly( const QString& vid, const QStringList& vparts )
        : id( vid ), parts( vparts )
        {}
};

// LDD conversion database
class LDrawXML
{
public:
    explicit LDrawXML() {}

    bool read( const QString& path );
    QString stats() const;
    const QString& errorString() const;

    int x2l_color( int lego ) const;
    QString x2l_id( int lego ) const;
    std::optional<Transformation> x2l_tr( const QString& ldraw ) const;

protected:
    bool readLDrawMapping();
    bool readMaterial();
    bool readBrick();
    bool readTransformation();
    bool readAssembly();
    bool readPart();

private:
    QXmlStreamReader m_xml;
    QMap<int, int> m_materials;
    QMap<int, QString> m_bricks;
    QMap<QString, Transformation> m_transformations;
    QMap<QString, Assembly> m_assemblies;
    QString m_error;
};

#endif
