/*
 *  Copyright 2017, 2020, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ldrawxml.h"

#include <QFile>
#include <QStringList>
#include <QVector3D>

#include "math_utils.h"

QString LDrawXML::stats() const
{
    const static QString FMT = QStringLiteral( "%1 materials, %2 bricks, %3 transformations, %4 assemblies" );
    return FMT.arg( m_materials.size() ).arg( m_bricks.size() ).arg( m_transformations.size() ).arg( m_assemblies.size() );
}

const QString& LDrawXML::errorString() const
{
    return m_error;
}

bool LDrawXML::read( const QString& path )
{
    QFile file( path );
    if ( !file.open( QFile::ReadOnly | QFile::Text ) ) {
        m_error += QStringLiteral( "Opening “%1” failed.\n" ).arg( path );
        m_error += file.errorString();
        return false;
    }
    m_xml.setDevice( &file );
    if ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "LDrawMapping" ) {
            if ( !readLDrawMapping() ) {
                return false;
            }
        } else {
            m_error += QStringLiteral( "Expecting element LDrawMapping, got “%1”." ).arg( m_xml.name().toString() );
            return false;
        }
    }
    file.close();
    return !m_xml.hasError();
}

bool LDrawXML::readLDrawMapping()
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "LDrawMapping" );

    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Material" ) {
            if ( !readMaterial() ) {
                return false;
            }
        } else if ( m_xml.name() == "Brick" ) {
            if ( !readBrick() ) {
                return false;
            }
        } else if ( m_xml.name() == "Transformation" ) {
            if ( !readTransformation() ) {
                return false;
            }
        } else if ( m_xml.name() == "Assembly"  ) {
            if ( !readAssembly() ) {
                return false;
            }
        } else {
            m_xml.skipCurrentElement();
        }
    }
    return true;
}

bool LDrawXML::readMaterial()
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "Material" );

    bool ok;
    const int lego = m_xml.attributes().value( QStringLiteral( "lego" ) ).toInt( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Material: lego attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "lego" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    const int ldraw = m_xml.attributes().value( "ldraw" ).toInt( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Material: ldraw attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "ldraw" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    m_materials.insert( lego, ldraw );
    m_xml.skipCurrentElement();
    return true;
}

bool LDrawXML::readBrick()
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "Brick" );

    bool ok;
    const int lego = m_xml.attributes().value( QStringLiteral( "lego" ) ).toInt( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Brick: lego attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "lego" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    m_bricks.insert( lego, m_xml.attributes().value( QStringLiteral( "ldraw" ) ).toString().toLower() );
    m_xml.skipCurrentElement();
    return true;
}

bool LDrawXML::readTransformation()
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "Transformation" );

    bool ok;
    const double tx = m_xml.attributes().value( QStringLiteral( "tx" ) ).toDouble( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Transformation: tx attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "tx" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    const double ty = m_xml.attributes().value( QStringLiteral( "ty" ) ).toDouble( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Transformation: ty attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "ty" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    const double tz = m_xml.attributes().value( QStringLiteral( "tz" ) ).toDouble( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Transformation: tz attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "tz" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    const double ax = m_xml.attributes().value( QStringLiteral( "ax" ) ).toDouble( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Transformation: ax attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "ax" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    const double ay = m_xml.attributes().value( QStringLiteral( "ay" ) ).toDouble( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Transformation: ay attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "ay" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    const double az = m_xml.attributes().value( QStringLiteral( "az" ) ).toDouble( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Transformation: az attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "az" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    const double angle = m_xml.attributes().value( QStringLiteral( "angle" ) ).toDouble( &ok );
    if ( !ok ) {
        m_error += QStringLiteral( "Transformation: angle attribute isn’t a number “%1”." ).arg( m_xml.attributes().value( QStringLiteral( "angle" ) ).toString() );
        m_error += QStringLiteral( "\nLine %1 column %2" ).arg( m_xml.lineNumber() ).arg( m_xml.columnNumber() );
        return false;
    }
    m_transformations.insert( m_xml.attributes().value( QStringLiteral( "ldraw" ) ).toString().toLower(),
                              Transformation( QVector3D( -tx, -ty, -tz  ),
                                              rot2mat( ax, ay, az, -180.0 * angle / M_PI ) )
        );
    m_xml.skipCurrentElement();
    return true;
}

bool LDrawXML::readAssembly()
{
    Q_ASSERT( m_xml.isStartElement() && m_xml.name() == "Assembly" );

    const QString id = m_xml.attributes().value( QStringLiteral( "lego" ) ).toString().toLower();
    QStringList parts;
    while ( m_xml.readNextStartElement() ) {
        if ( m_xml.name() == "Part" ) {
            parts << m_xml.attributes().value( QStringLiteral( "ldraw" ) ).toString().toLower();
            m_xml.skipCurrentElement();
        } else {
            m_xml.skipCurrentElement();
        }
    }
    m_assemblies.insert( id, Assembly( id, parts ) );
    return true;
}

int LDrawXML::x2l_color( int lego ) const
{
    if ( m_materials.contains( lego ) ) {
        return m_materials.value( lego );
    }
    return lego;
}

QString LDrawXML::x2l_id( int lego ) const
{
    static const QString FMT = QStringLiteral( "%1.dat" );
    if ( m_bricks.contains( lego ) ) {
        return m_bricks.value( lego );
    }
    return FMT.arg( lego );
}

std::optional<Transformation> LDrawXML::x2l_tr( const QString& ldraw ) const
{
    if ( !m_transformations.contains( ldraw ) ) {
        return {};
    }
    return m_transformations.value( ldraw );
}
