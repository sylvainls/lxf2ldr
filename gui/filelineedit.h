/*
 *  Copyright 2018  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FILELINEEDIT_H
#define FILELINEEDIT_H

#include <QtWidgets>

class FileLineEdit : public QWidget
{
    Q_OBJECT

public:
    explicit FileLineEdit( QWidget* window=0,
                           const QString& title="",
                           const QString& dir="",
                           const QString& value="",
                           const QString& filters="",
                           bool open=true,
                           QWidget* parent=0 );
    QString value() const;

private slots:
    void lineChanged( const QString& text );
    void buttonPressed();

signals:
    void valueChanged( const QString& text );

private:
    QWidget* m_window;
    QLineEdit* m_line;
    QString m_title;
    QString m_dir;
    QString m_filters;
    bool m_open;
};

#endif //  FILEINPUTLINE_H
