/*
 *  Copyright 2018  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "optionalwidget.h"

OptionalWidget::OptionalWidget( const QString& text,
                                QWidget* child,
                                QWidget* parent )
    : QWidget( parent ),
      m_child( child )
{
    QHBoxLayout* layout = new QHBoxLayout();
    QCheckBox* chk = new QCheckBox( text );
    layout->addWidget( chk );
    layout->addWidget( m_child );
    setLayout( layout );
    m_child->setEnabled( false );

    connect( chk, &QCheckBox::stateChanged, this, &OptionalWidget::stateChanged );
}

QWidget* OptionalWidget::child() const
{
    return m_child;
}

void OptionalWidget::stateChanged( int state )
{
    m_child->setEnabled( state == Qt::Checked );
    if ( state != Qt::Checked ) {
        emit hideValue();
    }
}
