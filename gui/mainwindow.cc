/*
 *  Copyright 2018, 2020, 2022  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"

#include <QtWidgets>
#include <QTextStream>

#include "common/ldrawxml.h"
#include "common/decors.h"
#include "common/flex.h"
#include "common/lxf.h"
#include "common/lxf2ldr.h"
#include "filelineedit.h"
#include "optionalwidget.h"

MainWindow::MainWindow()
{
    readSettings();
    create();
    setUnifiedTitleAndToolBarOnMac( true );
}

void MainWindow::closeEvent( QCloseEvent* event )
{
    writeSettings();
    event->accept();
}

void MainWindow::processFile()
{
    // read LDD conversion database
    LDrawXML ldrawxml;
    if ( !ldrawxml.read( m_ldraw_xml ) ) {
        QMessageBox::critical( this, tr( "Error reading database" ),
                               tr( "Couldn’t read conversion file “%1”. Aborting.\n" ).arg( m_ldraw_xml ) + ldrawxml.errorString() );
        return;
    }
    // read decoration conversion database
    Decors decors;
    if ( !decors.read( m_decors ) ) {
        QMessageBox::critical( this, tr( "Error reading Decors" ),
                               tr( "Couldn’t read decoration conversion file “%1”. Aborting.\n" ).arg( m_decors ) + decors.errorString() );
        return;
    }
    // read flexible parts conversion database
    Flex flex;
    if ( !flex.read( m_flex ) ) {
        QMessageBox::critical( this, tr( "Error reading Flex" ),
                               tr( "Couldn’t read flexible parts conversion file “%1”. Aborting.\n" ).arg( m_flex ) + flex.errorString() );
        return;
    }
    // read file
    LXF lxf;
    if ( !lxf.read( m_input_file, m_instructions ) ) {
        QMessageBox::critical( this, tr( "Error reading LXF" ),
                               tr( "Couldn’t read “%1”. Aborting.\n" ).arg( m_input_file ) + lxf.errorString() );
        return;
    }
    // open output
    QFile fout( m_output_file );
    if ( !fout.open( QFile::WriteOnly | QFile::Truncate ) ) {
        QMessageBox::critical( this, tr( "Error opening output" ),
                               tr( "Couldn’t read “%1”. Aborting.\n" ).arg( m_output_file ) );
        return;
    }
    // convert LXF file
    QTextStream tout( &fout );
    lxf2ldr( lxf, tout, m_instructions, m_mark_decor, ldrawxml, decors, flex );
    QMessageBox::information( this, tr( "Done" ),
                              tr( "Converted “%1” to “%2”\n" ).arg( m_input_file ).arg( m_output_file ) + lxf.stats() );
}

void MainWindow::about()
{
    QMessageBox::about( this, tr( "About gui_l2l" ),
                        tr( "Minimal graphical interface for lxf2ldr, a tool "
                            "to convert LDD files to LDraw.\n"
                            "Copyright 2017-2022 Sylvain Sauvage" ) );
}

void MainWindow::setLDrawXml( const QString& value )
{
    m_ldraw_xml = value;
}

void MainWindow::setDecors( const QString& value )
{
    m_decors = value;
}

void MainWindow::setFlex( const QString& value )
{
    m_flex = value;
}

void MainWindow::setMarkDecor( const QString& value )
{
    bool ok;
    const int color = value.toInt( &ok );
    if ( ok && color >= 0 ) {
        m_mark_decor = color;
    } else {
        m_mark_decor = 0;
    }
}

void MainWindow::setInputFile( const QString& value )
{
    m_input_file = value;
    m_input_dir = QFileInfo( m_input_file ).dir().absolutePath();
}

void MainWindow::setOutputFile( const QString& value )
{
    m_output_file = value;
    m_output_dir = QFileInfo( m_output_file ).dir().absolutePath();
}

void MainWindow::resetLDrawXml()
{
    m_ldraw_xml = QStringLiteral( ":/ldraw.xml" );
}

void MainWindow::resetDecors()
{
    m_decors = QStringLiteral( ":/decors_lxf2ldr.yaml" );
}

void MainWindow::resetFlex()
{
    m_flex = QStringLiteral( ":/flex_lxf2ldr.yaml" );
}

void MainWindow::resetMarkDecor()
{
    m_mark_decor = 0;
}

void MainWindow::setInstructions( bool checked )
{
    m_instructions = checked;
}

void MainWindow::resetInstructions()
{
    m_instructions = false;
}

void MainWindow::create()
{
    QMenu* fileMenu = menuBar()->addMenu( tr( "&File" ) );
    const QIcon exitIcon = QIcon::fromTheme( "application-exit" );
    QAction* exitAct = fileMenu->addAction( exitIcon, tr( "E&xit" ), this, &QWidget::close );
    exitAct->setShortcuts( QKeySequence::Quit );
    exitAct->setStatusTip( tr( "Exit the application" ) );

    QMenu* helpMenu = menuBar()->addMenu( tr( "&Help" ) );
    QAction* aboutAct = helpMenu->addAction( tr( "&About" ), this, &MainWindow::about );
    aboutAct->setStatusTip( tr( "Show the About box" ) );

    QVBoxLayout* layout = new QVBoxLayout();
    FileLineEdit* input = new FileLineEdit( this, tr( "Input File" ), m_input_dir, m_input_file,
                                            tr( "LXF files (*.lxf);;any file (*.*)" ) );
    FileLineEdit* output = new FileLineEdit( this, tr( "Output File" ), m_output_dir, m_output_file,
                                             tr( "LDraw files (*.ldr *.mpd);;any file (*.*)" ) );
    FileLineEdit* ldrawxml = new FileLineEdit( this, tr( "ldraw.xml Conversion File" ), "", "",
                                               tr( "XML files (*.xml);;any file (*.*)" ) );
    OptionalWidget* oldraw = new OptionalWidget( "", ldrawxml );
    FileLineEdit* decors = new FileLineEdit( this, tr( "Decors Conversion File" ), "", "",
                                             tr( "YAML files (*.yaml);;any file (*.*)" ) );
    OptionalWidget* odecors = new OptionalWidget( "", decors );
    FileLineEdit* flex = new FileLineEdit( this, tr( "Flex Conversion File" ), "", "",
                                           tr( "YAML files (*.yaml);;any file (*.*)" ) );
    OptionalWidget* oflex = new OptionalWidget( "", flex );
    QLineEdit* markdecor = new QLineEdit();
    OptionalWidget* omark = new OptionalWidget( tr( "Unknown decorated parts color:" ), markdecor );
    QCheckBox* useinstr = new QCheckBox( tr( "Use LDD-generated instructions when present." ), this );

    QPushButton* process = new QPushButton( tr( "GO!" ) );

    layout->addWidget( input );
    layout->addWidget( oldraw );
    layout->addWidget( odecors );
    layout->addWidget( oflex );
    layout->addWidget( omark );
    layout->addWidget( output );
    layout->addWidget( useinstr );
    layout->addWidget( process );

    QWidget* central = new QWidget();
    central->setLayout( layout );
    setCentralWidget( central );

    connect( input,     &FileLineEdit::valueChanged, this, &MainWindow::setInputFile );
    connect( output,    &FileLineEdit::valueChanged, this, &MainWindow::setOutputFile );
    connect( ldrawxml,  &FileLineEdit::valueChanged, this, &MainWindow::setLDrawXml );
    connect( decors,    &FileLineEdit::valueChanged, this, &MainWindow::setDecors );
    connect( flex,      &FileLineEdit::valueChanged, this, &MainWindow::setFlex );
    connect( markdecor, &QLineEdit::textChanged,     this, &MainWindow::setMarkDecor );
    connect( useinstr,  &QCheckBox::toggled,         this, &MainWindow::setInstructions );
    connect( oldraw,    &OptionalWidget::hideValue,  this, &MainWindow::resetLDrawXml );
    connect( odecors,   &OptionalWidget::hideValue,  this, &MainWindow::resetDecors );
    connect( oflex,     &OptionalWidget::hideValue,  this, &MainWindow::resetFlex );
    connect( omark,     &OptionalWidget::hideValue,  this, &MainWindow::resetMarkDecor );
    connect( process,   &QAbstractButton::released,  this, &MainWindow::processFile );
}

void MainWindow::readSettings()
{
    QSettings settings;
    const QByteArray geometry = settings.value( "geometry", QByteArray() ).toByteArray();
    if ( geometry.isEmpty() ) {
        const QRect availableGeometry = QApplication::desktop()->availableGeometry( this );
        resize( availableGeometry.width() / 3, availableGeometry.height() / 2 );
        move( ( availableGeometry.width() - width() ) / 2,
              ( availableGeometry.height() - height() ) / 2 );
    } else {
        restoreGeometry( geometry );
    }
    m_ldraw_xml = settings.value( "ldraw_xml", ":/ldraw.xml" ).toString();
    m_decors = settings.value( "decors", ":/decors_lxf2ldr.yaml" ).toString();
    m_flex = settings.value( "flex", ":/flex_lxf2ldr.yaml" ).toString();
    m_mark_decor = settings.value( "mark_decor", 0 ).toInt();
    m_input_dir = settings.value( "last_input_dir", QString() ).toString();
    m_output_dir = settings.value( "last_output_dir", QString() ).toString();
    m_instructions = settings.value( "use_instructions", false ).toBool();
}

void MainWindow::writeSettings()
{
    QSettings settings;
    settings.setValue( "geometry", saveGeometry() );
    settings.setValue( "ldraw_xml", m_ldraw_xml );
    settings.setValue( "decors", m_decors );
    settings.setValue( "flex", m_flex );
    settings.setValue( "mark_decor", m_mark_decor );
    settings.setValue( "use_instructions", m_instructions );
    settings.setValue( "last_input_dir", m_input_dir );
    settings.setValue( "last_output_dir", m_output_dir );
}
