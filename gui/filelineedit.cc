/*
 *  Copyright 2018  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "filelineedit.h"

FileLineEdit::FileLineEdit( QWidget* window,
                            const QString& title,
                            const QString& dir,
                            const QString& value,
                            const QString& filters,
                            bool open,
                            QWidget* parent )
    : QWidget( parent ),
      m_window( window ),
      m_title( title ),
      m_dir( dir ),
      m_filters( filters ),
      m_open( open )
{
    m_line = new QLineEdit( value );
    QPushButton* button = new QPushButton( "…" );
    QLabel* text = new QLabel( title );
    
    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget( text );
    layout->addWidget( m_line );
    layout->addWidget( button );
    setLayout( layout );

    connect( m_line, &QLineEdit::textChanged, this, &FileLineEdit::lineChanged );
    connect( button, &QPushButton::released, this, &FileLineEdit::buttonPressed );
}

void FileLineEdit::lineChanged( const QString& text )
{
    emit valueChanged( text );
}

void FileLineEdit::buttonPressed()
{
    QFileDialog dialog( m_window, m_title, m_dir, m_filters );
    dialog.setWindowModality( Qt::WindowModal );
    dialog.setAcceptMode( m_open ? QFileDialog::AcceptOpen : QFileDialog::AcceptSave );
    if ( dialog.exec() != QDialog::Accepted )
        return;
    m_line->setText( dialog.selectedFiles().first() );
}

QString FileLineEdit::value() const
{
    return m_line->text();
}
