TEMPLATE = app
TARGET = gui_l2l

! include( ../common/common.pri ) {
    error( "Couldn’t find the common.pri file!" )
}
INCLUDEPATH += .
QT += widgets
requires(qtConfig(filedialog))

HEADERS += filelineedit.h optionalwidget.h mainwindow.h
SOURCES += filelineedit.cc optionalwidget.cc mainwindow.cc main.cc
