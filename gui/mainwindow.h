/*
 *  Copyright 2018, 2020  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();

protected:
    void closeEvent( QCloseEvent* event ) override;

private slots:
    void processFile();
    void about();
    void setLDrawXml( const QString& value );
    void setDecors( const QString& value );
    void setFlex( const QString& value );
    void setMarkDecor( const QString& value );
    void setInputFile( const QString& value );
    void setOutputFile( const QString& value );
    void setInstructions( bool checked );
    void resetLDrawXml();
    void resetDecors();
    void resetFlex();
    void resetMarkDecor();
    void resetInstructions();

private:
    void create();
    void readSettings();
    void writeSettings();

    QString m_ldraw_xml;
    QString m_decors;
    QString m_flex;
    int     m_mark_decor;
    QString m_input_file;
    QString m_output_file;
    QString m_input_dir;
    QString m_output_dir;
    bool    m_instructions;
};

#endif // MAINWINDOW_H
