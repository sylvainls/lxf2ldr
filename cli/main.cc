/*
 *  Copyright 2017, 2020  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QTextStream>

#include "common/ldrawxml.h"
#include "common/decors.h"
#include "common/flex.h"
#include "common/lxf.h"
#include "common/lxf2ldr.h"

int main( int argc, char** argv )
{
    // default databases
    Q_INIT_RESOURCE( resources );
    const static QString LDRAW_XML( ":/ldraw.xml" );
    const static QString DECORS( ":/decors_lxf2ldr.yaml" );
    const static QString FLEX( ":/flex_lxf2ldr.yaml" );

    // handling arguments
    QCoreApplication app( argc, argv );
    QCoreApplication::setApplicationName( "lxf2ldr" );
    QCoreApplication::setApplicationVersion( "2.1" );

    QCommandLineParser parser;
    parser.setApplicationDescription( QStringLiteral( "Translate LXF file(s) in LDraw format to standard output." ) );
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption verbose( QStringList() << "V" << "verbose",
                                QStringLiteral( "Print loading info on standard error." ) );
    parser.addOption( verbose );
    QCommandLineOption ldraw_xml_path( QStringList() << "l" << "ldrawxml",
                                       QStringLiteral( "Set LDD’s conversion file name and path (%1)." ).arg( LDRAW_XML ),
                                       "filepath",
                                       LDRAW_XML );
    parser.addOption( ldraw_xml_path );
    QCommandLineOption decors_path( QStringList() << "d" << "decors",
                                    QStringLiteral( "Set decoration conversion file name and path (%1)." ).arg( DECORS ),
                                    "filepath",
                                    DECORS );
    parser.addOption( decors_path );
    QCommandLineOption flex_path( QStringList() << "f" << "flex",
                                  QStringLiteral( "Set flexible parts conversion file name and path (%1)." ).arg( FLEX ),
                                  "filepath",
                                  FLEX );
    parser.addOption( flex_path );
    QCommandLineOption markdecor( QStringList() << "m" << "markdecor",
                                  QStringLiteral( "Color parts that should be decored but are not." ),
                                  "colornbr",
                                  "0" );
    parser.addOption( markdecor );
    QCommandLineOption instructions( QStringList() << "i" << "instructions",
                                     QStringLiteral( "Use built-in instructions if present." ) );
    parser.addOption( instructions );

    parser.addPositionalArgument( QStringLiteral( "LXF…" ), QStringLiteral( "LXF file(s) to translate into LDR format." ) );

    // process arguments
    parser.process( app );

    const bool be_verbose = parser.isSet( verbose );

    const bool use_instructions = parser.isSet( instructions );

    const QStringList lxf_names = parser.positionalArguments();
    if ( lxf_names.size() < 1 ) {
        parser.showHelp( 1 );
        return 1;
    }

    QTextStream cerr( stderr );
    // read LDD conversion database
    LDrawXML ldrawxml;
    if ( !ldrawxml.read( parser.value( ldraw_xml_path ) ) ) {
        cerr << QStringLiteral( "Couldn’t read conversion file “%1”. Aborting." ).arg( parser.value( ldraw_xml_path ) ) << Qt::endl;
        cerr << ldrawxml.errorString() << Qt::endl;
        return 2;
    }
    if ( be_verbose ) {
        cerr << parser.value( ldraw_xml_path ).section( '/', -1 ) << ": " << ldrawxml.stats() << Qt::endl;
    }

    bool ok;
    const int mark_color = parser.value( markdecor ).toInt( &ok );
    if ( !ok ) {
        cerr << QStringLiteral( "Mark color should be a number for an LDraw color, not “%1”." ).arg( parser.value( markdecor ) ) << Qt::endl;
        return 3;
    }

    // read decoration conversion database
    Decors decors;
    if ( !decors.read( parser.value( decors_path ) ) ) {
        cerr << QStringLiteral( "Couldn’t read decoration conversion file “%1”. Aborting. " ).arg( parser.value( decors_path ) ) << Qt::endl;
        cerr << decors.errorString() << Qt::endl;
        return 4;
    }
    if ( be_verbose ) {
        cerr << parser.value( decors_path ).section( '/', -1 ) << ": " << decors.stats() << Qt::endl;
    }

    // read flexible parts conversion database
    Flex flex;
    if ( !flex.read( parser.value( flex_path ) ) ) {
        cerr << QStringLiteral( "Couldn’t read flexible parts conversion file “%1”. Aborting. " ).arg( parser.value( flex_path ) ) << Qt::endl;
        cerr << flex.errorString() << Qt::endl;
        return 4;
    }
    if ( be_verbose ) {
        cerr << parser.value( flex_path ).section( '/', -1 ) << ": " << flex.stats() << Qt::endl;
    }

    // convert LXF files to stdout;
    QTextStream cout( stdout );
    for ( const QString& lxf_path: lxf_names ) {
        LXF lxf;
        if ( !lxf.read( lxf_path, use_instructions ) ) {
            cerr << QStringLiteral( "Couldn’t read “%1”. Aborting." ).arg( lxf_path ) << Qt::endl;
            cerr << lxf.errorString() << Qt::endl;
            return 8;
        }
        if ( be_verbose ) {
            cerr << lxf_path.section( '/', -1 ) << ": " << lxf.stats() << Qt::endl;
        }
        lxf2ldr( lxf, cout, use_instructions, mark_color, ldrawxml, decors, flex );
    }
    return 0;
}
